package pl.mago.braincode.braincode.api.server;

import pl.mago.braincode.braincode.model.Categories;
import pl.mago.braincode.braincode.model.CategoriesContener;
import pl.mago.braincode.braincode.model.Session;
import pl.mago.braincode.braincode.model.User;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Path;
import retrofit.http.Query;

/**
 * Created by macbookpro on 14.03.15.
 */
public interface ServerUserModule {

    @GET("/token?grant_type=client_credentials")
    Session getSession();

    @FormUrlEncoded
    @POST("/login")
    User login(@Field("access_token") String token, @Field("userLogin") String login, @Field("hashPass") String password);

    @GET("/categories")
    Categories categories(@Query("access_token") String token);

    @POST("/User/{user_id}/{login}/{password}")
    String addUser(@Path("user_id") Integer userId, @Path("login") String login, @Path("password") String password);


}
