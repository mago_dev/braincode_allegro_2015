package pl.mago.braincode.braincode.categories;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import pl.mago.braincode.braincode.R;
import pl.mago.braincode.braincode.api.UserModule;
import pl.mago.braincode.braincode.core.CoreFragment;
import pl.mago.braincode.braincode.core.CoreLoadDataAsyncTask;
import pl.mago.braincode.braincode.model.Categories;
import pl.mago.braincode.braincode.model.CategoriesContener;
import pl.mago.braincode.braincode.model.Session;

/**
 * Created by macbookpro on 14.03.15.
 */
public class CategoriesFragment extends CoreFragment {

    public static final String TAG = CategoriesFragment.class.getSimpleName();
    private ListView mMenuListView;
    private CategoriesAdapter mCatAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.categories_fragment_view, container, false);

        mMenuListView = (ListView) v.findViewById(R.id.cat_list);
        mCatAdapter = new CategoriesAdapter(getActivity());

        new CoreLoadDataAsyncTask(getActivity(), new CoreLoadDataAsyncTask.LoadDataInBackground<Session>() {
            @Override
            public Session onLoadInBackground(Object... args) {
                return UserModule.getInstance().getSession();
            }

            @Override
            public void onLoadResultSuccess(Session result) {
                getCategories(result.getAccessToken());
            }

            @Override
            public void onLoadResultFailure(String error) {

            }
        }).execute();

        return v;
    }

    private void getCategories(final String accessToken) {
        new CoreLoadDataAsyncTask(getActivity(), new CoreLoadDataAsyncTask.LoadDataInBackground<Categories>() {
            @Override
            public Categories onLoadInBackground(Object... args) {
                return UserModule.getInstance().getCategories((String) args[0]);
            }

            @Override
            public void onLoadResultSuccess(Categories result) {
                if (result != null) {
                    mCatAdapter.setItems(result.getCategories());
                    mMenuListView.setAdapter(mCatAdapter);
                }
            }

            @Override
            public void onLoadResultFailure(String error) {

            }
        }).execute(accessToken);
    }

}
