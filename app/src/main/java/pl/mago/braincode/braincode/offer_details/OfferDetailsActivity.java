package pl.mago.braincode.braincode.offer_details;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import pl.mago.braincode.braincode.R;
import pl.mago.braincode.braincode.core.CoreActivity;
import pl.mago.braincode.braincode.main.OffertsListFragment;

public class OfferDetailsActivity extends CoreActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_offer_details);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        int offerId = -1;
        if (getIntent() != null){
            offerId = getIntent().getIntExtra(OfferDetailsFragment.EXTRA_OFFER_ID, -1);
        }

        OfferDetailsFragment fragment = new OfferDetailsFragment();
        if (offerId > 0){
            fragment.setOffer(offerId);
        }

        replaceFragment(R.id.offers_content_content, fragment, OfferDetailsFragment.TAG, false);
    }

}
