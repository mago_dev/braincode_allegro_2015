package pl.mago.braincode.braincode.api.server;

import java.util.List;

import pl.mago.braincode.braincode.model.Offer;
import pl.mago.braincode.braincode.model.Session;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Path;

/**
 * Created by macbookpro on 14.03.15.
 */
public interface ServerOfferModule {

    @GET("/GetOffers")
    List<Offer> getOffers();

    @GET("/GetOffer/{offer_id}/{user_id}")
    Offer getOffer(@Path("offer_id") Integer offerId, @Path("user_id") Integer userId );

    @POST("/Contract/{offer_id}/{user_id}/{count}")
    String contract(@Path("offer_id") Integer offerId, @Path("user_id") Integer userId, @Path("count") Integer count );

}
