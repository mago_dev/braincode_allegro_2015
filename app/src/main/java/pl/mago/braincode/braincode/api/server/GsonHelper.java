package pl.mago.braincode.braincode.api.server;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.lang.reflect.Type;

/**
 * Created by macbookpro on 14.03.15.
 */
public class GsonHelper {

    private final Gson mGson;

    private static GsonHelper mInstance;

    public static GsonHelper getInstance() {
        if (mInstance == null) {
            mInstance = new GsonHelper();
        }
        return mInstance;
    }

    private GsonHelper() {
        this.mGson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().serializeNulls().create();
    }

    public Gson getGson() {
        return this.mGson;
    }

    public String toJson(Object obj) {
        return this.mGson.toJson(obj);
    }

    public <T> T fromJson(String json, Class<T> classOfT) {
        return this.mGson.fromJson(json, classOfT);
    }

    public <T> T fromJson(String json, Type classOfT) {
        return this.mGson.fromJson(json, classOfT);
    }
}
