package pl.mago.braincode.braincode.api;

import java.util.List;

import pl.mago.braincode.braincode.api.server.ServerImp;
import pl.mago.braincode.braincode.api.server.ServerOfferModule;
import pl.mago.braincode.braincode.model.Offer;

/**
 * Created by macbookpro on 14.03.15.
 */
public class OffersModule {

    private ServerOfferModule mService;
    private static OffersModule mInstance = null;

    public static OffersModule getInstance() {
        synchronized (OffersModule.class) {
            if (mInstance == null) {
                mInstance = new OffersModule();
            }
        }
        return mInstance;
    }

    private OffersModule() {
        mService = ServerImp.getInstance()
                .getRestAdapter()
                .create(ServerOfferModule.class);
    }

    public List<Offer> getOffers() {
        return mService.getOffers();
    }

    public Offer getOffer(Integer offerId, Integer userId) {
        return mService.getOffer(offerId, userId);
    }

    public String contract(Integer offerId, Integer userId) {
        return mService.contract(offerId, userId, 1);
    }

}
