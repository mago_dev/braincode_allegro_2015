package pl.mago.braincode.braincode.api;

import pl.mago.braincode.braincode.api.server.ServerImp;
import pl.mago.braincode.braincode.api.server.ServerUserModule;
import pl.mago.braincode.braincode.model.Categories;
import pl.mago.braincode.braincode.model.CategoriesContener;
import pl.mago.braincode.braincode.model.Session;
import pl.mago.braincode.braincode.model.User;
import pl.mago.braincode.braincode.util.StringHelper;

/**
 * Created by macbookpro on 14.03.15.
 */
public class UserModule {

    private final ServerUserModule mApiService;
    private ServerUserModule mAllegroService;
    private static UserModule mInstance = null;

    public static UserModule getInstance() {
        synchronized (UserModule.class) {
            if (mInstance == null) {
                mInstance = new UserModule();
            }
        }
        return mInstance;
    }

    private UserModule() {
        mAllegroService = ServerImp.getInstance()
                .getAllegroRestAdapter()
                .create(ServerUserModule.class);

        mApiService = ServerImp.getInstance()
                .getRestAdapter()
                .create(ServerUserModule.class);
    }

    public Session getSession() {
        ServerUserModule mService = ServerImp.getInstance()
                .getSessionRestAdapter()
                .create(ServerUserModule.class);

        return mService.getSession();
    }

    public User login(String token, String l, String p) {
        return mAllegroService.login(token, l, StringHelper.getHashFromPassword(p));
    }

    public Categories getCategories(String token) {
        return mAllegroService.categories(token);
    }

    public String addUser(int userId, String login, String password) {
        return mApiService.addUser(userId, login, password);
    }

}
