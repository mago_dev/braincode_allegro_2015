package pl.mago.braincode.braincode.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by macbookpro on 14.03.15.
 */
public class Offer {

    @SerializedName("id")
    @Expose
    private Integer apiId;
    @Expose
    private String image;
    @Expose
    private String title;
    @Expose
    private String description;
    @Expose
    private Integer sellerId;
    @Expose
    private Double price;
    @Expose
    private Double regularPrice;
    @Expose
    private Integer amountToSell;
    @Expose
    private Integer endTime;
    @Expose
    private Integer contractedUnits;

    /**
     * @return The image
     */
    public String getImage() {
        return image;
    }

    /**
     * @param image The image
     */
    public void setImage(String image) {
        this.image = image;
    }

    public Offer withImage(String image) {
        this.image = image;
        return this;
    }

    /**
     * @return The title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title The title
     */
    public void setTitle(String title) {
        this.title = title;
    }

    public Offer withTitle(String title) {
        this.title = title;
        return this;
    }

    /**
     * @return The description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description The description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    public Offer withDescription(String description) {
        this.description = description;
        return this;
    }

    /**
     * @return The sellerId
     */
    public Integer getSellerId() {
        return sellerId;
    }

    /**
     * @param sellerId The sellerId
     */
    public void setSellerId(Integer sellerId) {
        this.sellerId = sellerId;
    }

    public Offer withSellerId(Integer sellerId) {
        this.sellerId = sellerId;
        return this;
    }

    /**
     * @return The price
     */
    public Double getPrice() {
        return price;
    }

    /**
     * @param price The price
     */
    public void setPrice(Double price) {
        this.price = price;
    }

    public Offer withPrice(Double price) {
        this.price = price;
        return this;
    }

    /**
     * @return The regularPrice
     */
    public Double getRegularPrice() {
        return regularPrice;
    }

    /**
     * @param regularPrice The regularPrice
     */
    public void setRegularPrice(Double regularPrice) {
        this.regularPrice = regularPrice;
    }

    public Offer withRegularPrice(Double regularPrice) {
        this.regularPrice = regularPrice;
        return this;
    }

    /**
     * @return The amountToSell
     */
    public Integer getAmountToSell() {
        return amountToSell;
    }

    /**
     * @param amountToSell The amountToSell
     */
    public void setAmountToSell(Integer amountToSell) {
        this.amountToSell = amountToSell;
    }

    public Offer withAmountToSell(Integer amountToSell) {
        this.amountToSell = amountToSell;
        return this;
    }

    /**
     * @return The endTime
     */
    public Integer getEndTime() {
        return endTime;
    }

    /**
     * @param endTime The endTime
     */
    public void setEndTime(Integer endTime) {
        this.endTime = endTime;
    }

    public Offer withEndTime(Integer endTime) {
        this.endTime = endTime;
        return this;
    }

    /**
     * @return The contractedUnits
     */
    public Integer getContractedUnits() {
        return contractedUnits;
    }

    /**
     * @param contractedUnits The contractedUnits
     */
    public void setContractedUnits(Integer contractedUnits) {
        this.contractedUnits = contractedUnits;
    }

    public Offer withContractedUnits(Integer contractedUnits) {
        this.contractedUnits = contractedUnits;
        return this;
    }

    public Integer getApiId() {
        return apiId;
    }

    public void setApiId(Integer apiId) {
        this.apiId = apiId;
    }
}
