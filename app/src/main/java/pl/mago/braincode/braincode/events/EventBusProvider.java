package pl.mago.braincode.braincode.events;

import de.greenrobot.event.EventBus;

/**
 * Created by macbookpro on 14.03.15.
 */
public class EventBusProvider {

    private static final EventBus EVENT_BUS = new EventBus();

    public static EventBus getInstance() {
        return EVENT_BUS;
    }

    private EventBusProvider() {
    }
}
