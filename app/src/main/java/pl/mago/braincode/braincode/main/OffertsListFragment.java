package pl.mago.braincode.braincode.main;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import pl.mago.braincode.braincode.R;
import pl.mago.braincode.braincode.api.OffersModule;
import pl.mago.braincode.braincode.api.UserModule;
import pl.mago.braincode.braincode.core.CoreFragment;
import pl.mago.braincode.braincode.core.CoreLoadDataAsyncTask;
import pl.mago.braincode.braincode.events.EventBusProvider;
import pl.mago.braincode.braincode.model.Offer;
import pl.mago.braincode.braincode.model.Session;

/**
 * Created by macbookpro on 13.03.15.
 */
public class OffertsListFragment extends CoreFragment implements AdapterView.OnItemClickListener, CoreLoadDataAsyncTask.LoadDataInBackground<List<Offer>> {

    public static final String TAG = OffertsListFragment.class.getSimpleName();

    private ListView mOffersListView;
    private OffersListAdapter mOfferAdapter;
    private CoreLoadDataAsyncTask mOffersTask;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.offers_list_view, container, false);

        mOffersListView = (ListView) v.findViewById(R.id.offers_list);
        mOffersListView.setOnItemClickListener(this);
        mOfferAdapter = new OffersListAdapter(getActivity());

        mOffersTask = new CoreLoadDataAsyncTask(getActivity(), this);
        mOffersTask.execute();

        return v;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        EventBusProvider.getInstance().post(mOfferAdapter.getItem(position));
    }

    @Override
    public List<Offer> onLoadInBackground(Object... args) {
        return OffersModule.getInstance().getOffers();
    }

    @Override
    public void onLoadResultSuccess(List<Offer> result) {
        if (result != null){
            mOfferAdapter.setItems(result);
            mOffersListView.setAdapter(mOfferAdapter);
        }
    }

    @Override
    public void onLoadResultFailure(String error) {

    }
}
