package pl.mago.braincode.braincode.core;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBarActivity;

import pl.mago.braincode.braincode.events.BaseEvent;
import pl.mago.braincode.braincode.events.EventBusProvider;

/**
 * Created by macbookpro on 14.03.15.
 */
public abstract class CoreActivity extends ActionBarActivity {

    public void replaceFragment(int viewId, Fragment fragment, String tag, boolean addToBackStack) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(viewId, fragment, tag);
        if (addToBackStack)
            transaction.addToBackStack(null);
        transaction.commit();
    }

    @Override
    protected void onResume() {
        super.onResume();
        EventBusProvider.getInstance().register(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        EventBusProvider.getInstance().unregister(this);
    }

    public void onEvent(BaseEvent e) {

    }

}
