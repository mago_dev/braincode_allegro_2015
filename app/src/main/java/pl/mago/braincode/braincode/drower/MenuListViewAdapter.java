package pl.mago.braincode.braincode.drower;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.lang.ref.WeakReference;
import java.util.List;

import pl.mago.braincode.braincode.R;

/**
 * Created by macbookpro on 13.03.15.
 */
public class MenuListViewAdapter extends BaseAdapter {

    private final WeakReference<Context> mContextRef;
    private List<MenuListItem> mItems;

    private class ViewHolder {
        ImageView icon;
        TextView label;
    }

    public MenuListViewAdapter(Context context) {
        mContextRef = new WeakReference<Context>(context);
    }

    public List<MenuListItem> getItems() {
        return mItems;
    }

    public void setItems(List<MenuListItem> items) {
        mItems = items;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return mItems == null ? 0 : mItems.size();
    }

    @Override
    public MenuListItem getItem(int position) {
        return mItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return mItems.get(position).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        if (convertView == null) {
            convertView = createView();
            holder = createHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        updateView(holder, position);

        return convertView;
    }

    private View createView() {
        return LayoutInflater.from(mContextRef.get()).inflate(R.layout.drawer_list_item, null);
    }

    private ViewHolder createHolder(View convertView) {
        ViewHolder holder = new ViewHolder();

        holder.icon = (ImageView) convertView.findViewById(R.id.drawer_item_icon);
        holder.label = (TextView) convertView.findViewById(R.id.drawer_item_label);
        return holder;
    }

    private void updateView(ViewHolder holder, int position) {
        MenuListItem menuListItem = mItems.get(position);
        holder.label.setText(menuListItem.getLabel());

            holder.icon.setImageResource(menuListItem.getIcon());
    }
}