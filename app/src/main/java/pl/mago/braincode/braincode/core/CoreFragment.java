package pl.mago.braincode.braincode.core;

import android.os.Bundle;
import android.support.v4.app.Fragment;

import pl.mago.braincode.braincode.events.BaseEvent;
import pl.mago.braincode.braincode.events.EventBusProvider;

/**
 * Created by macbookpro on 14.03.15.
 */
public abstract class CoreFragment extends Fragment {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EventBusProvider.getInstance().register(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        EventBusProvider.getInstance().unregister(this);
    }

    public void onEvent(BaseEvent e) {

    }

}
