package pl.mago.braincode.braincode.offer_details;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import pl.mago.braincode.braincode.R;
import pl.mago.braincode.braincode.api.OffersModule;
import pl.mago.braincode.braincode.core.CoreFragment;
import pl.mago.braincode.braincode.core.CoreLoadDataAsyncTask;
import pl.mago.braincode.braincode.model.Offer;
import pl.mago.braincode.braincode.util.SettingsManager;
import pl.mago.braincode.braincode.util.StringHelper;

/**
 * Created by macbookpro on 14.03.15.
 */
public class OfferDetailsFragment extends CoreFragment implements View.OnClickListener, CoreLoadDataAsyncTask.LoadDataInBackground<Offer> {

    public static final String TAG = OfferDetailsFragment.class.getSimpleName();
    public static final String EXTRA_OFFER_ID = "extra_offer_id";

    private int mOfferId;
    private TextView mOfferTitle;
    private TextView mRegularPrize;
    private TextView mOfferPrize;
    private Button mBuyButton;
    private TextView mDescPrize;
    private ImageView mImageView;
    private TextView mPersonTextView;

    private CoreLoadDataAsyncTask RefreshTask;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.offers_details_view, container, false);

        mOfferTitle = (TextView) v.findViewById(R.id.offers_item_title);
        mImageView = (ImageView) v.findViewById(R.id.offer_image);
        mRegularPrize = (TextView) v.findViewById(R.id.offer_regilar_prize);
        mOfferPrize = (TextView) v.findViewById(R.id.offer_prize);
        mBuyButton = (Button) v.findViewById(R.id.buy_button);
        mBuyButton.setOnClickListener(this);
        mPersonTextView = (TextView) v.findViewById(R.id.offer_persons);
        mDescPrize = (TextView) v.findViewById(R.id.offers_item_desc);

        if (mOfferId > 0) {
            RefreshTask = new CoreLoadDataAsyncTask(getActivity(), this);
            RefreshTask.execute(mOfferId, 1);
        }

        return v;
    }

    private void setContent(Offer offer) {
        if (offer != null) {
            mOfferTitle.setText(offer.getTitle());
            mOfferPrize.setText(offer.getPrice() + "zł");
            mRegularPrize.setText(offer.getRegularPrice() + "zł");
            mDescPrize.setText(offer.getDescription());

            Bitmap imageBitmap = StringHelper.base64ToImage(offer.getImage());
            mImageView.setImageBitmap(imageBitmap);

            String conctracted = (offer.getContractedUnits() != null) ? "" + offer.getContractedUnits() : "0";
            mPersonTextView.setText(conctracted + "/" + offer.getAmountToSell());
        }
    }

    public void setOffer(int offer) {
        mOfferId = offer;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.buy_button:
                buy();
                break;
        }

    }

    private void buy() {
        int userId = SettingsManager.getInstance(getActivity()).getUserUid();
        if (userId > 0) {
            new CoreLoadDataAsyncTask(getActivity(), new CoreLoadDataAsyncTask.LoadDataInBackground<String>() {
                @Override
                public String onLoadInBackground(Object... args) {
                    return OffersModule.getInstance().contract((Integer) args[0], (Integer) args[1]);
                }

                @Override
                public void onLoadResultSuccess(String result) {
                    refreshView();
                }

                @Override
                public void onLoadResultFailure(String error) {
                }
            }).execute(mOfferId, SettingsManager.getInstance(getActivity()).getUserUid());
        } else {
            Toast.makeText(getActivity(), "Musisz być zalogowany aby móc kupić", Toast.LENGTH_SHORT).show();
        }
    }

    private void refreshView(){
        if (mOfferId > 0) {
            RefreshTask = new CoreLoadDataAsyncTask(getActivity(), this);
            RefreshTask.execute(mOfferId, 1);
        }
    }

    @Override
    public Offer onLoadInBackground(Object... args) {
        return OffersModule.getInstance().getOffer((Integer) args[0], (Integer) args[1]);
    }

    @Override
    public void onLoadResultSuccess(Offer result) {
        if (result != null) {
            setContent(result);
        }

    }

    @Override
    public void onLoadResultFailure(String error) {

    }
}
