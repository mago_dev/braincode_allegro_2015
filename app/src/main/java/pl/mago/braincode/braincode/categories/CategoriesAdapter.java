package pl.mago.braincode.braincode.categories;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.lang.ref.WeakReference;
import java.util.List;

import pl.mago.braincode.braincode.R;
import pl.mago.braincode.braincode.model.Category;

/**
 * Created by macbookpro on 14.03.15.
 */
public class CategoriesAdapter extends BaseAdapter {

    private final WeakReference<Context> mContextRef;
    private List<Category> mItems;

    private class ViewHolder {
        TextView label;
    }

    public CategoriesAdapter(Context context) {
        mContextRef = new WeakReference<Context>(context);
    }

    public List<Category> getItems() {
        return mItems;
    }

    public void setItems(List<Category> items) {
        mItems = items;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return mItems == null ? 0 : mItems.size();
    }

    @Override
    public Category getItem(int position) {
        return mItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        if (convertView == null) {
            convertView = createView();
            holder = createHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        updateView(holder, position);

        return convertView;
    }

    private View createView() {
        return LayoutInflater.from(mContextRef.get()).inflate(R.layout.drawer_list_item, null);
    }

    private ViewHolder createHolder(View convertView) {
        ViewHolder holder = new ViewHolder();

        holder.label = (TextView) convertView.findViewById(R.id.drawer_item_label);
        return holder;
    }

    private void updateView(ViewHolder holder, int position) {
        Category category = mItems.get(position);
        holder.label.setText(category.getName());
    }
}