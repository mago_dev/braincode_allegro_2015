package pl.mago.braincode.braincode.main;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.lang.ref.WeakReference;
import java.util.List;

import pl.mago.braincode.braincode.R;
import pl.mago.braincode.braincode.model.Offer;
import pl.mago.braincode.braincode.util.StringHelper;

/**
 * Created by macbookpro on 14.03.15.
 */
public class OffersListAdapter extends BaseAdapter {

    private final WeakReference<Context> mContextRef;
    private List<Offer> mItems;

    private class ViewHolder {
        TextView label;
        ImageView image;
        TextView prize;
        TextView people;
        TextView timeLeft;

    }

    public OffersListAdapter(Context context) {
        mContextRef = new WeakReference<Context>(context);
    }

    public List<Offer> getItems() {
        return mItems;
    }

    public void setItems(List<Offer> items) {
        mItems = items;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return mItems == null ? 0 : mItems.size();
    }

    @Override
    public Offer getItem(int position) {
        return mItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return mItems.get(position).getApiId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        if (convertView == null) {
            convertView = createView();
            holder = createHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        updateView(holder, position);

        return convertView;
    }

    private View createView() {
        return LayoutInflater.from(mContextRef.get()).inflate(R.layout.offers_list_item, null);
    }

    private ViewHolder createHolder(View convertView) {
        ViewHolder holder = new ViewHolder();

        holder.label = (TextView) convertView.findViewById(R.id.offers_item_title);
        holder.image = (ImageView) convertView.findViewById(R.id.offer_image);
        holder.prize = (TextView) convertView.findViewById(R.id.offer_prize);
        holder.people = (TextView) convertView.findViewById(R.id.offer_people);
        holder.timeLeft = (TextView) convertView.findViewById(R.id.offer_date);
        return holder;
    }

    private void updateView(ViewHolder holder, int position) {
        Offer menuListItem = mItems.get(position);

        if (menuListItem.getTitle() != null) {
            holder.label.setText(menuListItem.getTitle());
        } else {
            holder.label.setText("");
        }
        if (menuListItem.getPrice() != null) {
            holder.prize.setText(menuListItem.getPrice() + "");
        } else {
            holder.prize.setText("");
        }
        if (menuListItem.getAmountToSell() != null) {
            String conctracted = (menuListItem.getContractedUnits() != null) ? "" + menuListItem.getContractedUnits() : "0";
            holder.people.setText(conctracted + "/" + menuListItem.getAmountToSell());
        } else {
            holder.people.setText("");
        }
        if (menuListItem.getEndTime() != null) {
            holder.timeLeft.setText(StringHelper.getDaysLeft(menuListItem.getEndTime()) + "");
        } else {
            holder.timeLeft.setText("");
        }
        if (menuListItem.getImage() != null) {
            holder.image.setImageBitmap(StringHelper.base64ToImage(menuListItem.getImage()));
        }


    }
}