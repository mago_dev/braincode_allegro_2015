package pl.mago.braincode.braincode.drower;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import pl.mago.braincode.braincode.R;
import pl.mago.braincode.braincode.events.EventBusProvider;

/**
 * Created by macbookpro on 13.03.15.
 */
public class DrawerMenuView extends FrameLayout implements AdapterView.OnItemClickListener {

    public static final int MENU_ITEM_ID_MAIN = 1;
    public static final int MENU_ITEM_ID_CATEGORIES = 2;
    public static final int MENU_ITEM_ID_BOOKED = 3;
    public static final int MENU_ITEM_ID_SEND = 4;
    public static final int MENU_ITEM_ID_MINE = 5;
    public static final int MENU_ITEM_ID_SETTINGS = 6;
    public static final int MENU_ITEM_ID_LOGIN = 7;

    private ListView mMenuListView;
    private MenuListViewAdapter mMenuListViewAdapter;

    public DrawerMenuView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context);
    }

    public DrawerMenuView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public DrawerMenuView(Context context) {
        super(context);
        init(context);
    }

    private void init(Context context) {
        LayoutInflater.from(context).inflate(R.layout.drawer_view, this);

        mMenuListView = (ListView) findViewById(R.id.menu_list);
        mMenuListView.setOnItemClickListener(this);

        mMenuListViewAdapter = new MenuListViewAdapter(getContext());
        mMenuListViewAdapter.setItems(getListItems());
        mMenuListView.setAdapter(mMenuListViewAdapter);
    }

    private List<MenuListItem> getListItems() {
        List<MenuListItem> menuItemList = new ArrayList<MenuListItem>();

        menuItemList.add(new MenuListItem(DrawerMenuView.MENU_ITEM_ID_MAIN, getContext().getString(R.string.drower_home), R.drawable.ic_home));
        menuItemList.add(new MenuListItem(DrawerMenuView.MENU_ITEM_ID_CATEGORIES, getContext().getString(R.string.drower_categories), R.drawable.ic_category));
        menuItemList.add(new MenuListItem(DrawerMenuView.MENU_ITEM_ID_BOOKED, getContext().getString(R.string.drower_booked), R.drawable.ic_booked));
        menuItemList.add(new MenuListItem(DrawerMenuView.MENU_ITEM_ID_SEND, getContext().getString(R.string.drower_send), R.drawable.ic_send));
        menuItemList.add(new MenuListItem(DrawerMenuView.MENU_ITEM_ID_MINE, getContext().getString(R.string.drower_my), R.drawable.ic_my));
        menuItemList.add(new MenuListItem(DrawerMenuView.MENU_ITEM_ID_LOGIN, getContext().getString(R.string.drower_login), R.drawable.ic_login));

        return menuItemList;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        EventBusProvider.getInstance().post(mMenuListViewAdapter.getItem(position));
    }

    public void destroy() {
        if (mMenuListView != null) {
            mMenuListView.setOnItemClickListener(null);
            mMenuListView.setAdapter(null);
            mMenuListView = null;
        }
    }


}
