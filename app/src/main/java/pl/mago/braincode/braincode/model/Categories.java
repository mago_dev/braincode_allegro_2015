package pl.mago.braincode.braincode.model;

import com.google.gson.annotations.Expose;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by macbookpro on 14.03.15.
 */
public class Categories {

    @Expose
    private List<Category> categories = new ArrayList<Category>();

    /**
     * @return The categories
     */
    public List<Category> getCategories() {
        return categories;
    }

    /**
     * @param categories The categories
     */
    public void setCategories(List<Category> categories) {
        this.categories = categories;
    }


}
