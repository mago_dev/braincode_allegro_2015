package pl.mago.braincode.braincode.model;

import com.google.gson.annotations.Expose;

/**
 * Created by macbookpro on 14.03.15.
 */
public class Category {

    @Expose
    private String id;
    @Expose
    private String name;
    @Expose
    private Integer count;

    /**
     * @return The id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id The id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return The name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return The count
     */
    public Integer getCount() {
        return count;
    }

    /**
     * @param count The count
     */
    public void setCount(Integer count) {
        this.count = count;
    }


}
