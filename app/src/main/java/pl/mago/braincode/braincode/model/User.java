package pl.mago.braincode.braincode.model;

import com.google.gson.annotations.Expose;

/**
 * Created by macbookpro on 14.03.15.
 */
public class User {

    @Expose
    private String userId;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
