package pl.mago.braincode.braincode.main;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.widget.DrawerLayout;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;

import pl.mago.braincode.braincode.R;
import pl.mago.braincode.braincode.categories.CategoriesFragment;
import pl.mago.braincode.braincode.core.CoreActivity;
import pl.mago.braincode.braincode.core.CoreFragment;
import pl.mago.braincode.braincode.drower.DrawerMenuView;
import pl.mago.braincode.braincode.drower.MenuListItem;
import pl.mago.braincode.braincode.login.LoginFragment;
import pl.mago.braincode.braincode.mine.MineOffersFragment;
import pl.mago.braincode.braincode.model.Offer;
import pl.mago.braincode.braincode.offer_details.OfferDetailsActivity;
import pl.mago.braincode.braincode.offer_details.OfferDetailsFragment;
import pl.mago.braincode.braincode.send.SendOfferFragment;

public class MainActivity extends CoreActivity {

    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mDrawerToggle;
    private DrawerMenuView mDrawerMenuView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.drawable.ic_drawer, R.string.app_name, R.string.app_name) {
            @Override
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                mDrawerLayout.bringChildToFront(drawerView);
                mDrawerLayout.requestLayout();
                super.onDrawerOpened(drawerView);
            }
        };
        mDrawerToggle.syncState();
        mDrawerLayout.setDrawerListener(mDrawerToggle);
        mDrawerMenuView = (DrawerMenuView) findViewById(R.id.drawer_holder);
        replaceFragment(R.id.main_activity_content, new OffertsListFragment(), OffertsListFragment.TAG, false);
    }

    public void onEvent(Offer offer) {
        Intent i = new Intent(this, OfferDetailsActivity.class);
        i.putExtra(OfferDetailsFragment.EXTRA_OFFER_ID, offer.getApiId());
        startActivity(i);
    }

    public void onEvent(MenuListItem menuListItem) {
        switch (menuListItem.getId()) {
            case DrawerMenuView.MENU_ITEM_ID_MAIN:
                replaceMainFragment(new OffertsListFragment(), OffertsListFragment.TAG);
                break;
            case DrawerMenuView.MENU_ITEM_ID_CATEGORIES:
                replaceMainFragment(new CategoriesFragment(), CategoriesFragment.TAG);
                break;
            case DrawerMenuView.MENU_ITEM_ID_BOOKED:
                replaceMainFragment(new OffertsListFragment(), OffertsListFragment.TAG);
                break;
            case DrawerMenuView.MENU_ITEM_ID_SEND:
                replaceMainFragment(new SendOfferFragment(), SendOfferFragment.TAG);
                break;
            case DrawerMenuView.MENU_ITEM_ID_MINE:
                replaceMainFragment(new MineOffersFragment(), MineOffersFragment.TAG);
                break;
            case DrawerMenuView.MENU_ITEM_ID_LOGIN:
                replaceMainFragment(new LoginFragment(), LoginFragment.TAG);
                break;
        }
        mDrawerLayout.closeDrawer(Gravity.LEFT);
    }

    private void replaceMainFragment(CoreFragment fragment, String tag) {
        replaceFragment(R.id.main_activity_content, fragment, tag, false);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mDrawerMenuView != null) {
            mDrawerMenuView.destroy();
            mDrawerMenuView = null;
        }
    }
}
