package pl.mago.braincode.braincode.drower;

/**
 * Created by macbookpro on 13.03.15.
 */
public class MenuListItem {

    private int mId;
    private int mIcon;
    private int mActiveIcon;
    private String mLabel;

    public MenuListItem() {

    }

    public MenuListItem(int id, String label, int icon) {
        this.mId = id;
        this.mLabel = label;
        this.mIcon = icon;
    }

    public MenuListItem(int id, String label, int icon, int activeIcon) {
        this.mId = id;
        this.mLabel = label;
        this.mIcon = icon;
        this.mActiveIcon = activeIcon;
    }


    public int getId() {
        return mId;
    }

    public void setId(int id) {
        mId = id;
    }

    public int getIcon() {
        return mIcon;
    }

    public void setIcon(int icon) {
        mIcon = icon;
    }

    public int getActiveIcon() {
        return mActiveIcon;
    }

    public void setActiveIcon(int activeIcon) {
        mActiveIcon = activeIcon;
    }

    public String getLabel() {
        return mLabel;
    }

    public void setLabel(String label) {
        mLabel = label;
    }

}
