package pl.mago.braincode.braincode.login;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import pl.mago.braincode.braincode.R;
import pl.mago.braincode.braincode.api.UserModule;
import pl.mago.braincode.braincode.core.CoreFragment;
import pl.mago.braincode.braincode.core.CoreLoadDataAsyncTask;
import pl.mago.braincode.braincode.model.Session;
import pl.mago.braincode.braincode.model.User;
import pl.mago.braincode.braincode.util.SettingsManager;

/**
 * Created by macbookpro on 14.03.15.
 */
public class LoginFragment extends CoreFragment implements View.OnClickListener {

    public static final String TAG = LoginFragment.class.getSimpleName();

    private EditText mLoginEditText;
    private EditText mPasswordEditText;
    private Button mLoginButton;
    private TextView mKeyLabel;

    private CoreLoadDataAsyncTask mLoginTask;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.login_view, container, false);

        mLoginEditText = (EditText) v.findViewById(R.id.login_edit_text);
        mPasswordEditText = (EditText) v.findViewById(R.id.password_edit_text);
        mLoginButton = (Button) v.findViewById(R.id.login_button);
        mLoginButton.setOnClickListener(this);
        mKeyLabel = (TextView) v.findViewById(R.id.key_label);

        mLoginEditText.setText("pyro7");
        mPasswordEditText.setText("DUPAdupa123");

        return v;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.login_button:
                if (validate()) {
                    getSession(mLoginEditText.getText().toString(), mPasswordEditText.getText().toString());
                }
                break;
        }
    }

    private void getSession(final String username, final String password) {
        new CoreLoadDataAsyncTask(getActivity(), new CoreLoadDataAsyncTask.LoadDataInBackground<Session>() {
            @Override
            public Session onLoadInBackground(Object... args) {
                return UserModule.getInstance().getSession();
            }

            @Override
            public void onLoadResultSuccess(Session result) {
                login(result.getAccessToken(), username, password);
            }

            @Override
            public void onLoadResultFailure(String error) {

            }
        }).execute();
    }

    private void login(String token, final String username, final String password) {
        new CoreLoadDataAsyncTask(getActivity(), new CoreLoadDataAsyncTask.LoadDataInBackground<User>() {
            @Override
            public User onLoadInBackground(Object... args) {
                return UserModule.getInstance().login((String) args[0], (String) args[1], (String) args[2]);
            }

            @Override
            public void onLoadResultSuccess(User result) {
                addUser(result, username, password);
            }

            @Override
            public void onLoadResultFailure(String error) {

            }
        }).execute(token, username, password);
    }

    private void addUser(final User user, final String username, final String password) {
        new CoreLoadDataAsyncTask(getActivity(), new CoreLoadDataAsyncTask.LoadDataInBackground<String>() {
            @Override
            public String onLoadInBackground(Object... args) {
                return UserModule.getInstance().addUser((int) args[0], (String) args[1], (String) args[2]);
            }

            @Override
            public void onLoadResultSuccess(String result) {
                SettingsManager.getInstance(getActivity()).setLoggedUserId(Integer.parseInt(user.getUserId()));
                Toast.makeText(getActivity(), "SUkCES", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onLoadResultFailure(String error) {
            }
        }).execute(Integer.parseInt(user.getUserId()), username, "pass");
    }

    private boolean validate() {
        boolean valid = true;
        if (mLoginEditText.getText().toString() == null && mLoginEditText.getText().toString().length() <= 0) {
            this.mLoginEditText.setError(this.getString(R.string.validation_text));
            valid = false;
        }
        if (mPasswordEditText.getText().toString() == null && mPasswordEditText.getText().toString().length() <= 0) {
            this.mPasswordEditText.setError(this.getString(R.string.validation_text));
            valid = false;
        }
        return valid;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (mLoginEditText != null) {
            mLoginEditText = null;
        }
        if (mPasswordEditText != null) {
            mPasswordEditText = null;
        }
        if (mLoginButton != null) {
            mLoginButton.setOnClickListener(null);
            mLoginButton = null;
        }
        if (mKeyLabel != null) {
            mKeyLabel = null;
        }
    }

}
