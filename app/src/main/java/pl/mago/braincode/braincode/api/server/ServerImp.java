package pl.mago.braincode.braincode.api.server;

import android.util.Base64;

import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.converter.GsonConverter;

/**
 * Created by macbookpro on 14.03.15.
 */
public class ServerImp {

    public final static String ALLEGRO_URL = "https://api.natelefon.pl/v1/allegro";
    public final static String ALLEGRO_SESSION_URL = "https://api.natelefon.pl/oauth";
    public final static String API_URL = "http://braincodemobile.azure-mobile.net";

    private static final String HEADER_AUTHORIZATION = "Authorization";
    private static final String HEADER_BASIC = "Basic ";
    private static final String HEADER_CONTENT = "Content-Type";
    private static final String HEADER_CONTENT_VALUE = "application/x-www-form-urlencoded";


    private static ServerImp mInstance;

    public static ServerImp getInstance() {
        synchronized (ServerImp.class) {
            if (mInstance == null)
                mInstance = new ServerImp();
        }
        return mInstance;
    }

    private ServerImp() {
        mInstance = this;
    }

    public RestAdapter getSessionRestAdapter() {
        return new RestAdapter.Builder()
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .setEndpoint(ServerImp.ALLEGRO_SESSION_URL)
                .setConverter(new GsonConverter(GsonHelper.getInstance().getGson()))
                .setRequestInterceptor(getSessionHeaders()).build();
    }

    private RequestInterceptor getSessionHeaders() {
//        final String credentials = username + ":" + password;
        final String credentials = "braincode.mobi.2015" + ":" + "smAamczp";
        return (new RequestInterceptor() {
            @Override
            public void intercept(RequestFacade request) {
                request.addHeader(HEADER_AUTHORIZATION, HEADER_BASIC + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP));
            }
        });
    }

    public RestAdapter getAllegroRestAdapter() {
        return new RestAdapter.Builder()
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .setEndpoint(ServerImp.ALLEGRO_URL)
                .setConverter(new GsonConverter(GsonHelper.getInstance().getGson()))
                .setRequestInterceptor(getHeaders())
                .build();
    }

    private RequestInterceptor getHeaders() {
        return (new RequestInterceptor() {
            @Override
            public void intercept(RequestFacade request) {
                request.addHeader(HEADER_CONTENT, HEADER_CONTENT_VALUE);
            }
        });
    }

    public RestAdapter getRestAdapter() {
        return new RestAdapter.Builder()
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .setEndpoint(ServerImp.API_URL)
                .setConverter(new GsonConverter(GsonHelper.getInstance().getGson()))
                .build();
    }

}
