package pl.mago.braincode.braincode.core;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;

import java.lang.ref.SoftReference;
import java.lang.ref.WeakReference;

import pl.mago.braincode.braincode.R;
import retrofit.RetrofitError;

/**
 * Created by macbookpro on 14.03.15.
 */
public class CoreLoadDataAsyncTask extends AsyncTask<Object, Object, Object> {

    public interface LoadDataInBackground<T> {
        public T onLoadInBackground(Object... args);

        public void onLoadResultSuccess(T result);

        public void onLoadResultFailure(String error);
    }

    private final WeakReference<Context> mContextRef;

    private final SoftReference<LoadDataInBackground> mLoadDataInBackground;
    private Dialog mLoadingDialog;
    private final int mDialogStyle;

    public CoreLoadDataAsyncTask(Context context, LoadDataInBackground loadDataInBackground) {
        this(context, loadDataInBackground, android.R.style.Theme_Holo_Light_NoActionBar);
    }

    public CoreLoadDataAsyncTask(Context context, LoadDataInBackground loadDataInBackground, int dialogStyle) {
        mContextRef = new WeakReference<Context>(context);
        this.mLoadDataInBackground = new SoftReference<LoadDataInBackground>(loadDataInBackground);
        this.mDialogStyle = dialogStyle;
    }

    @Override
    protected void onPreExecute() {
        mLoadingDialog = showLoadingDialog(mContextRef.get());
        super.onPreExecute();
    }

    @Override
    protected Object doInBackground(Object... args) {
        try {
            return mLoadDataInBackground.get().onLoadInBackground(args);
        } catch (RetrofitError error){
            error.printStackTrace();
            return error;
        }
    }

    @Override
    protected void onPostExecute(Object result) {
        if (result instanceof RetrofitError) {
            LoadDataInBackground background = mLoadDataInBackground.get();
            if (background != null) {
                background.onLoadResultFailure((String) result);
            }
        } else {
            LoadDataInBackground background = mLoadDataInBackground.get();
            if (background != null) {
                background.onLoadResultSuccess(result);
            }
        }
        destroyDialog();
        super.onPostExecute(result);
    }

    @Override
    protected void onCancelled() {
        destroyDialog();
        super.onCancelled();
    }

    private Dialog showLoadingDialog(Context context) {
        if (context != null) {
            Dialog dialog = new Dialog(context, mDialogStyle);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(0));
            dialog.setCancelable(false);
            dialog.setContentView(R.layout.loading_dialog);
            dialog.show();
            return dialog;
        } else {
            return null;
        }
    }

    private void destroyDialog() {
        if (mLoadingDialog != null) {
            if (mLoadingDialog.isShowing()) {
                mLoadingDialog.dismiss();
            }
            mLoadingDialog = null;
        }
    }

    public void destroy() {
        destroyDialog();
        this.cancel(true);
    }

}
