package pl.mago.braincode.braincode.send;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import pl.mago.braincode.braincode.R;
import pl.mago.braincode.braincode.core.CoreFragment;

/**
 * Created by macbookpro on 14.03.15.
 */
public class SendOfferFragment extends CoreFragment {

    public static final String TAG = SendOfferFragment.class.getSimpleName();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.send_fragment_view, container, false);

        return v;
    }

}
