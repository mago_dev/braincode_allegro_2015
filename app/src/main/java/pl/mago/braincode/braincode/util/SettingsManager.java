package pl.mago.braincode.braincode.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * Created by macbookpro on 14.03.15.
 */
public class SettingsManager {

    private static final String KEY_USER_ID = "user_id";

    private static SettingsManager sInstance;

    public static SettingsManager getInstance(Context context) {
        synchronized (SettingsManager.class) {
            if (sInstance == null) {
                sInstance = new SettingsManager(context);
            }
        }
        return sInstance;
    }

    private final SharedPreferences mSharedPreferences;

    private SettingsManager(Context context) {
        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
    }


    protected synchronized boolean getBooleanSetting(String key, boolean defValue) {
        return mSharedPreferences.getBoolean(key, defValue);
    }

    protected synchronized String getStringSetting(String key, String defValue) {
        return mSharedPreferences.getString(key, defValue);
    }

    protected synchronized int getIntSetting(String key, int defValue) {
        return mSharedPreferences.getInt(key, defValue);
    }

    protected synchronized float getFloatSetting(String key, int defValue) {
        return mSharedPreferences.getFloat(key, defValue);
    }

    protected synchronized void setBooleanSetting(String key, boolean value) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putBoolean(key, value);
        editor.commit();
    }

    protected synchronized void setIntSetting(String key, int value) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putInt(key, value);
        editor.commit();
    }


    public void setLoggedUserId(int id) {
        setIntSetting(SettingsManager.KEY_USER_ID, id);
    }

    public int getUserUid() {
        return getIntSetting(SettingsManager.KEY_USER_ID, -1);
    }


}
