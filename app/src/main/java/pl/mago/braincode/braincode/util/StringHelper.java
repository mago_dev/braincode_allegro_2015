package pl.mago.braincode.braincode.util;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * Created by macbookpro on 14.03.15.
 */
public class StringHelper {

    public static String getHashFromPassword(String hashPass) {
        MessageDigest digest = null;
        try {
            digest = MessageDigest.getInstance("SHA-256");
            byte[] hash = digest.digest(hashPass.getBytes("UTF-8"));
            return  Base64.encodeToString(hash, Base64.NO_WRAP);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static Bitmap base64ToImage(String photoBase64) {
        photoBase64 = photoBase64.substring(photoBase64.indexOf(",") + 1, photoBase64.length());
        byte[] photo = Base64.decode(photoBase64, Base64.DEFAULT);
        Bitmap bitmap = BitmapFactory.decodeByteArray(photo, 0, photo.length);
        return bitmap;
    }

    public static long getDaysLeft(long timestamp){
        Calendar startDate = Calendar.getInstance();

        Calendar endDate = Calendar.getInstance();
        Date d = new Date(timestamp * 1000);
        endDate.setTime(d);

        long end = endDate.getTimeInMillis();
        long start = startDate.getTimeInMillis();
        return TimeUnit.MILLISECONDS.toDays(Math.abs(end - start));
    }

}
